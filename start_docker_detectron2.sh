#!/bin/bash

docker run --rm --shm-size=4096m --gpus all --init -it -v /home/kyle/Cityscapes\ Dataset/sequence/:/root/detectron2/data/cityscapes/ -v /home/kyle/samba_share:/root/samba_share detectron2 bash
