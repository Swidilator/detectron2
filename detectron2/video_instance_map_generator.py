import detectron2
from cityscapes import CityscapesDemoVideoDataset
from torch.utils.data import Dataset, DataLoader
import os

import torch
from torchvision import transforms

# import some common libraries
import numpy as np
from PIL import Image
from tqdm import tqdm
import random

from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg

if __name__ == '__main__':
    CONFIG_FILE: str = "Cityscapes/mask_rcnn_R_50_FPN.yaml"

    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file(CONFIG_FILE))
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(CONFIG_FILE)
    model: torch.nn.Module = detectron2.model_zoo.get(CONFIG_FILE, trained=True)
    model = model.cuda()
    model = model.eval()
    # predictor = DefaultPredictor(cfg)
    # predictor.model.eval()

    split: str = "val"

    def collate_fn(data: list, dim=1):
        dict_list = [x[0] for x in data]
        file_name_list: list = [x[1] for x in data]
        return dict_list, file_name_list

    # dataset = CityscapesDemoVideoDataset(f"/root/detectron2/data/cityscapes/leftImg8bit_sequence/{split}")
    dataset = CityscapesDemoVideoDataset(f"/home/kyle/samba_share/cityscapes_dataset/sequence/leftImg8bit_sequence/{split}")
    dataloader = DataLoader(dataset, batch_size=2, collate_fn=collate_fn, shuffle=False)

    transform = transforms.ToPILImage()
    # output_dir: str = "/root/samba_share/generated_instance_ids_train/"
    # if not os.path.exists(output_dir):
    #     print(f"Making output dir: {os.path.abspath(output_dir)}")
    #     os.makedirs(output_dir, exist_ok=True)

    for i, (images, file_names) in tqdm(enumerate(dataloader), total=len(dataloader.dataset) / dataloader.batch_size):
        if i < 2175:
            continue
        with torch.no_grad():
            print("")
            output: torch.Tensor = model(images)

            for j in range(len(images)):
                img_file_name: str = file_names[j]
                single_output: torch.Tensor = output[j]
                img_mask: torch.Tensor = single_output['instances'].pred_masks.int().cpu()

                print(f"{len(single_output['instances'].pred_classes)}: Processing: {img_file_name}")

                full_folder_path: str = "/home/kyle/samba_share/generated_instance_ids/{split}/{folder_name}".format(
                    folder_name=img_file_name.split("_")[0],
                    split=split,
                )
                os.makedirs(full_folder_path, exist_ok=True)

                img_path: str = "{full_folder_path}/{file_name}".format(
                    full_folder_path=full_folder_path,
                    file_name=img_file_name,
                )
                instance_img_path: str = img_path.replace("leftImg8bit", "gtFine_instanceIds")

                try:
                    instance_img = torch.argmax(img_mask, 0).cpu().numpy().astype(np.uint8)
                except RuntimeError:
                    print()
                    instance_img = torch.zeros_like(images[0]["image"][0]).cpu().numpy().astype(np.uint8)

                instance_img: Image.Image = Image.fromarray(instance_img)
                instance_img.save(instance_img_path)
