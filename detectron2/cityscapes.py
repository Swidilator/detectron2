import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import os
from detectron2.data.detection_utils import read_image
#import cv2
import numpy as np

from PIL import Image


class CityscapesDemoVideoDataset(Dataset):
    def __init__(self, demoVideo_dir: str):

        self.demoVideo_dir: str = demoVideo_dir
        self.image_file_names: list = []
        self.image_file_paths: list = []

        self.transform = transforms.Compose(
            {
                transforms.ToTensor()
            }
        )

        for city in os.listdir(self.demoVideo_dir):
            if city != "munster":
                continue
            img_dir: str = os.path.join(self.demoVideo_dir, city)

            for file_name in os.listdir(img_dir):
                self.image_file_names.append(file_name)
                self.image_file_paths.append(os.path.join(img_dir, file_name))

        self.image_file_paths = sorted(self.image_file_paths)
        self.image_file_names = sorted(self.image_file_names)

        self.__len: int = len(self.image_file_paths)

    def __len__(self):
        return self.__len

    def __getitem__(self, item):
        if item < 2175:
            return None, None
        img: Image.Image = read_image(self.image_file_paths[item], format="BGR")
        # img_array: np.ndarray = np.array(img)
        # print("img_array shape", img_array.shape)
        #img_tensor: torch.Tensor = (self.transform(img) * 255).int()
        img_tensor: torch.Tensor = torch.tensor(img.copy()).permute((2, 0, 1)).int()
        # a,b,c = torch.chunk(img_tensor, 3, dim=0)
        # img_tensor = torch.cat((c,b,a), dim=0)
        out_dict = {
            "image": img_tensor
        }

        return out_dict, self.image_file_names[item]

